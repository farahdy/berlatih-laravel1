<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Form Sign Up</title>
</head>
<body>
    <div>
        <h1>Buat Account Baru!</h1>
        <h2>Sign Up Form</h2>
    </div>

    <form action="/welcome" method="POST">
        @csrf
        <label for="fnama">First name:</label><br><br>
        <input type="text" id="fnama" name="first">
        <br><br>
        @csrf
        <label for="lnama">Last name:</label><br><br>
        <input type="text" id="lnama" name="last">
        <br><br>

        <label>Gender:</label><br><br>
        <input type="radio" name="gender" value="0">Male <br>
        <input type="radio" name="gender" value="1" checked>Female <br>
        <input type="radio" name="gender" value="2">Other
        <br><br>

        <label>Nationality:</label><br><br>
        <select>
            <option value="0">Indonesia</option>
            <option value="1">Amerika</option>
            <option value="2">Korea</option>
            <option value="3">Jerman</option>
        </select>
        <br><br>

        <label>Language Spoken: </label><br><br>
        <input type="checkbox" name="bahasa" value="0" checked>Bahasa Indonesia <br>
        <input type="checkbox" name="bahasa" value="1">English <br>
        <input type="checkbox" name="bahasa" value="2">Other
        <br><br>

        <label for="bio">Bio:</label><br><br>
        <textarea cols="35" rows="10" id="bio"></textarea>
        <br>
        <input type="submit" value="Sign Up">
    </form>
    <p> &copy; Farah </p>
</body>
</html>